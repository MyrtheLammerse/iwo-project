#!/bin/bash

#Myrthe Lammerse, s2772841

#This shell script accesses the twitter file from 1 March 2017 12:00 and analyzes the tweets of this specific time.

#First it unpacks the twitter file from 1 March 2017 12:00, then it shows the tweets with one tweet per line, and then it counts the lines.
echo "The number of tweets in the sample is:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

#First it unpacks the twitter file from 1 March 2017 12:00, then it shows the tweets with one tweet per line, next it sorts and filters out the duplicates and at last the number of lines are counted.
echo "The number of unique tweets in the sample is:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l

#First it unpacks the twitter file from 1 March 2017 12:00, then it shows the tweets with one tweet per line, next it sorts and filters out the duplicates, then it searches for every tweet that startes with RT and then the number of lines are counted.
echo "The number of unique retweets in the sample is:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep '^RT' | wc -l

#First it unpacks the twitter file from 1 March 2017 12:00, then it shows the tweets with one tweet per line, next it sorts and filters out the duplicates, then it removes all the tweets that do start with RT and at last it shows the first 20 tweets. 
echo "The first 20 unique tweets in the sample that are not retweet are:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -v '^RT' | head -n20