#!/bin/bash

TWEETS=~/Documents/IWO-Project/tweets.out.gz
TWEET2TAB=~/Documents/IWO-Project/tweet2tab

# Every single time, a statement about which party is being investigated is echoed. After that the tweets are loaded. 
# Then every tweet is placed on a new line. After the tweets are loaded, the python script that will determine the gender
# is called. Then it will state if it either searches for tweets written by males or by females. 
# After the gender is determined, the political party is grepped. After all the tweets containing that
# party are grepped, it will cancel out the tweets that are negative about that party. Last but not least, 
# the lines are counted and the results are gettin echoed. 

echo "The number of tweets about the VVD that are written by men are:"
VVDmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'vvd' | grep -v -i 'anti vvd\|geen vvd' | wc -l)"
echo $VVDmale

echo "The number of tweets about the VVD that are written by women are:"
VVDfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'vvd' | grep -v -i 'anti vvd\|geen vvd' | wc -l)"
echo $VVDfemale

echo "The number of tweets about the PvdA that are written by men are:"
PvdAmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'pvda\|partij van de arbeid' | grep -v -i 'anti pvda\|anti partij van de arbeid\|geen pvda\|geen partij van de arbeid' | wc -l)"
echo $PvdAmale

echo "The number of tweets about the PvdA that are written by women are:"
PvdAfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'pvda\|partij van de arbeid' | grep -v -i 'anti pvda\|anti partij van de arbeid\|geen pvda\|geen partij van de arbeid' | wc -l)"
echo $PvdAfemale

echo "The number of tweets about the PVV that are written by men are:"
PVVmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'pvv\|partij voor de vrijheid' | grep -v -i 'anti pvv\|anti partij voor de vrijheid\|geen pvv\|geen partij voor de vrijheid' | wc -l)"
echo $PVVmale

echo "The number of tweets about the PVV that are written by women are:"
PVVfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'pvv\|partij voor de vrijheid' | grep -v -i 'anti pvv\|anti partij voor de vrijheid\|geen pvv\|geen partij voor de vrijheid' | wc -l)"
echo $PVVfemale

echo "The number of tweets about the SP that are written by men are:"
SPmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'sp' | grep -v -i 'anti sp\|geen sp' | wc -l)"
echo $SPmale

echo "The number of tweets about the SP that are written by women are:"
SPfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'sp' | grep -v -i 'anti geen sp\|geen sp' | wc -l)"
echo $SPfemale

echo "The number of tweets about the CDA that are written by men are:"
CDAmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'cda' | grep -v -i 'anti cda\|geen cda' | wc -l)"
echo $CDAmale

echo "The number of tweets about the CDA that are written by women are:"
CDAfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'cda' | grep -v -i 'anti cda\|geen cda' | wc -l)"
echo $CDAfemale

echo "The number of tweets about D66 that are written by men are:"
D66male="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'd66' | grep -v -i 'anti d66\|geen d66' | wc -l)"
echo $D66male

echo "The number of tweets about D66 that are written by women are:"
sD66female="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'd66' | grep -v -i 'anti d66\|geen d66' | wc -l)"
echo $D66female

echo "The number of tweets about the CU that are written by men are:"
CUmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'cu\|christenunie\|christen unie' | grep -v -i 'anti cu\|geen cu\|anti christenunie\|geen christenunie\|anti christen unie\|geen christen unie' | wc -l)"
echo $CUmale

echo "The number of tweets about the CU that are written by women are:"
CUfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'cu\|christenunie\|christen unie' | grep -v -i 'anti cu\|geen cu\|anti christenunie\|geen christenunie\|anti christen unie\|geen christen unie' | wc -l)"
echo $CUfemale

echo "The number of tweets about GroenLinks that are written by men are:"
GLmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'groenlinks\|gl' | grep -v -i 'anti groenlinks\|anti gl\|geen groenlinks\| geen gl' | wc -l)"
echo $GLmale

echo "The number of tweets about GroenLinks that are written by women are:"
GLfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'groenlinks' | grep -v -i 'anti groenlinks\|anti gl\|geen groenlinks\| geen gl' | wc -l)"
echo $GLfemale

echo "The number of tweets about the SGP that are written by men are:"
SGPmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'sgp' | grep -v -i 'anti sgp\|geen sgp' | wc -l)"
echo $SGPmale

echo "The number of tweets about the SGP that are written by women are:"
SGPfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'sgp' | grep -v -i 'anti sgp\|geen sgp' | wc -l)"
echo $SGPfemale

echo "The number of tweets about the PvvD that are written by men are:"
PVVDmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'pvvd\|partij voor de dieren' | grep -v -i 'anti pvvd\|geen pvvd\|anti partij voor de dieren\|geen partij voor de dieren' | wc -l)"
echo $PVVDmale

echo "The number of tweets about the PvvD that are written by women are:"
PVVDfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'pvvd\|partij voor de dieren' | grep -v -i 'anti pvvd\|geen pvvd\|anti partij voor de dieren\|geen partij voor de dieren' | wc -l)"
echo $PVVDfemale

echo "The number of tweets about 50Plus that are written by men are:"
PLUSmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i '50plus\|50 plus\|50+\|50 +' | grep -v -i 'anti 50plus\|geen 50plus\|anti 50 plus\|geen 50 plus\|anti 50+\|geen 50+\|anti 50 +\|geen 50 +' | wc -l)"
echo $PLUSmale

echo "The number of tweets about 50Plus that are written by women are:"
PLUSfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i '50plus\|50 plus\|50+\|50 +' | grep -v -i 'anti 50plus\|geen 50plus\|anti 50 plus\|geen 50 plus\|anti 50+\|geen 50+\|anti 50 +\|geen 50 +' | wc -l)"
echo $PLUSfemale

echo "The number of tweets about DENK that are written by men are:"
DENKmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'denk' | grep -v -i 'anti denk\|geen denk\|ik denk\|denk jij' | wc -l)"
echo $DENKmale

echo "The number of tweets about DENK that are written by women are:"
DENKfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'denk' | grep -v -i 'anti denk\|geen denk\|ik denk\|denk jij' | wc -l)"
echo $DENKfemale

echo "The number of tweets about the Forum voor Democratie that are written by men are:"
FVDmale="$(zless "$TWEETS"  | "$TWEET2TAB" text | python get-gender.py | grep '^male' | grep -i 'fvd\|forum voor democratie' | grep -v -i 'anti fvd\|geen fvd\|anti forum voor democratie\|geen forum voor democratie' | wc -l)"
echo $FVDmale

echo "The number of tweets about the Forum voor Democratie that are written by women are:"
FVDfemale="$(zless "$TWEETS"  | "$TWEET2TAB" text  | python get-gender.py | grep '^female' | grep -i 'fvd\|forum voor democratie' | grep -v -i 'anti fvd\|geen fvd\|anti forum voor democratie\|geen forum voor democratie' | wc -l)"
echo $FVDfemale
